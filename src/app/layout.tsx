import Providers from '@/components/Providers'
import { Noto_Sans_Thai } from 'next/font/google';
import './globals.css'

// Done after the video and optional: add page metadata
export const metadata = {
  title: 'D²Chat',
  description: 'Instant live chat with one click with google account and no more registration',
}

const noto = Noto_Sans_Thai({
  subsets: ['latin'],
  variable: '--font-noto',
  weight: ['100', '300', '400', '500', '700', '900'],
});

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang='en'>
      <body className={`${noto.variable}`}>
        <Providers>{children}</Providers>
      </body>
    </html>
  )
}
